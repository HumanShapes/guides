ZSH Setup
====================

Setting up your Terminal to be pretty and useful.


Install ZSH
-------------------------------

If you used [Laptop](https://github.com/thoughtbot/laptop) to set-up your computer, you should already have ZSH installed.

Install [Oh-My-ZSH](https://github.com/robbyrussell/oh-my-zsh)
-------------------------------

     curl -L http://install.ohmyz.sh | sh

Add Plugins
-------------------------------

Go to: ```~/.oh-my-zsh``` and edit ```oh-my-zsh.sh```

Change plugins to:

    plugins=(git,atom,bower,battery,cloudapp,colorize,comman-aliases,brew,bundler,github,osx)

A full list of plugins can be found here: https://github.com/robbyrussell/oh-my-zsh/tree/master/plugins

Set your Theme
-------------------------------

Change theme to:

    ZSH_THEME="sunrise"

A full list of themes can be found here: https://github.com/robbyrussell/oh-my-zsh/wiki/Themes

In Terminal go to ```Terminal > Preferences > Profiles [Basic] > Window``` and change your backround color. Then go to ```Test``` and adjust your "Text" and "Bold Text" colors.
